"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const nb_1 = require("./nb");
function partition(data, k) {
    let parts = [...Array(k).keys()].map(i => {
        return [];
    });
    data.forEach((x, idx) => {
        const partIdx = idx % k;
        parts[partIdx].push(x);
    });
    return parts;
}
exports.partition = partition;
function testing(summ, training, testing) {
    let testingResult = {
        tp: 0,
        tn: 0,
        fp: 0,
        fn: 0
    };
    testing.forEach(x => {
        const actual = x.keterangan;
        const nbResult = nb_1.nbWithSumm(summ, x);
        const predicted = nbResult.result;
        // TP
        if (actual === 1 && predicted === 1) {
            testingResult.tp += 1;
            // TN
        }
        else if (actual === 0 && predicted === 0) {
            testingResult.tn += 1;
            // FP
        }
        else if (actual === 0 && predicted === 1) {
            testingResult.fp += 1;
            // FN
        }
        else if (actual === 1 && predicted === 0) {
            testingResult.fn += 1;
        }
        else {
            throw new Error("What the fuck happen!");
        }
    });
    return Object.assign({ accuracy: exports.accuracy(testingResult), precision: exports.precision(testingResult), recall: exports.recall(testingResult), miscRate: exports.miscRate(testingResult) }, testingResult);
}
exports.testing = testing;
function testingAndChange(summ, training, testing, changeFunc) {
    return __awaiter(this, void 0, void 0, function* () {
        for (let x of testing) {
            const actual = x.keterangan;
            const nbResult = nb_1.nbWithSumm(summ, x);
            const predicted = nbResult.result;
            // Just remove the false positive
            if (actual === 0 && predicted === 1) {
                yield changeFunc(x._id, predicted);
            }
        }
    });
}
exports.testingAndChange = testingAndChange;
exports.accuracy = testR => (testR.tp + testR.tn) * 100.0 / (testR.tp + testR.tn + testR.fp + testR.fn);
exports.precision = testR => testR.tp * 100.0 / (testR.tp + testR.tn);
exports.recall = testR => testR.tp * 100.0 / (testR.tp + testR.fn);
exports.miscRate = testR => (testR.fp + testR.fn) * 100.0 / (testR.tp + testR.tn + testR.fp + testR.fn);
//# sourceMappingURL=testing.js.map