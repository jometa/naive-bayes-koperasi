"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongodb_1 = require("mongodb");
const testing_1 = require("../services/testing");
const summary_1 = require("../services/summary");
const assert = require("assert");
const ROUTE_PREFIX = "/testing";
function default_1(server, opts, next) {
    return __awaiter(this, void 0, void 0, function* () {
        const collection = server.mongo.db.collection("permintaan");
        server.post("/testing", (request, reply) => __awaiter(this, void 0, void 0, function* () {
            const k = parseInt(request.body.k);
            const dataset = request.body.dataset;
            let results = [];
            let items = yield (collection.find({ dataset }).toArray());
            const parts = testing_1.partition(items, k);
            for (let i = 0; i < k; i++) {
                // Declare the variables with 'let'. Capture the closure.
                let trainPart = trainingPart(parts, i);
                let testPart = parts[i];
                let summ = summary_1.summary(trainPart);
                let partResult = yield testing_1.testing(summ, trainPart, testPart);
                results.push(partResult);
            }
            reply.send(results);
        }));
        server.post("/testing-with-change", (request, reply) => __awaiter(this, void 0, void 0, function* () {
            const k = parseInt(request.body.k);
            const dataset = request.body.dataset;
            const findResult = yield collection.find({ dataset });
            const items = yield findResult.toArray();
            assert(items.length !== 0);
            const parts = testing_1.partition(items, k);
            var currentTask = Promise.resolve(1);
            parts.forEach((part, idx) => {
                // Capture the closures.
                let trainingData = parts.filter((p, pidx) => pidx !== idx);
                trainingData = trainingData.reduce((a, b) => a.concat(b), []);
                let summ = summary_1.summary(trainingData);
                // Run each testing in order.
                currentTask = currentTask.then(() => testingUntilAccuracyIsGood(summ, trainingData, part, idx));
            });
            yield currentTask;
            reply.send("OK");
        }));
        function testingUntilAccuracyIsGood(summ, training_part, testing_part, pidx) {
            return __awaiter(this, void 0, void 0, function* () {
                let partResult = testing_1.testing(summ, training_part, testing_part);
                if (partResult.accuracy < 65) {
                    console.log(`[INFO] changing partition-${pidx}`);
                    console.log(`[INFO] accuracy before changing partition-${pidx}: ${partResult.accuracy}`);
                    yield testing_1.testingAndChange(summ, training_part, testing_part, changeFunc);
                    partResult = testing_1.testing(summ, training_part, testing_part);
                    console.log(`[INFO] accuracy after changing partition-${pidx}: ${partResult.accuracy}`);
                }
                return partResult;
            });
        }
        function changeFunc(id, keterangan) {
            return __awaiter(this, void 0, void 0, function* () {
                const result = yield collection.findOneAndUpdate({ _id: new mongodb_1.ObjectID(id) }, { $set: {
                        keterangan
                    } });
                assert(result.value !== null);
            });
        }
        function trainingPart(parts, idxTrain) {
            return parts.filter((p, pidx) => pidx !== idxTrain).reduce((a, b) => a.concat(b), []);
        }
    });
}
exports.default = default_1;
//# sourceMappingURL=testing.js.map